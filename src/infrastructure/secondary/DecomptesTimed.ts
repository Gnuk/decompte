import { Consumer, Decomptes, Unsubscribe } from '../../domain/Decompte';

export class DecomptesTimed implements Decomptes {
  launch(timed: Consumer<Date>): Unsubscribe {
    const interval = setInterval(() => {
      timed(new Date());
    }, 1000);
    return () => clearInterval(interval);
  }
}
