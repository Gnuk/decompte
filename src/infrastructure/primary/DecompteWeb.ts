import { NOT_STARTED, OVER, Time } from '../../domain/Decompte';
import { DecompteShow } from '../../domain/DecompteShow';

const toHuman = (time: Time): string => {
  switch (time) {
    case OVER:
      return 'Over';
    case NOT_STARTED:
      return 'Not started';
    default:
      return `${time.toString()} min`;
  }
};

export const decompteWeb =
  (selector: HTMLElement): DecompteShow =>
  (time) => {
    selector.innerText = toHuman(time);
  };
