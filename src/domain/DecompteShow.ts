import { Time } from './Decompte';

export type DecompteShow = (time: Time) => void;
