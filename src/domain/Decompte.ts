export const OVER = Symbol('Over');
export const NOT_STARTED = Symbol('Not started');
export type Minutes = number;

export type Time = typeof OVER | Minutes | typeof NOT_STARTED;

export type Unsubscribe = () => void;

export type Consumer<T> = (subject: T) => void;

export interface IntervalBuilder {
  start: Date;
  end: Date;
}

class Interval {
  private readonly start: Date;
  private readonly end: Date;
  private constructor(builder: IntervalBuilder) {
    this.start = builder.start;
    this.end = builder.end;
  }

  static of(builder: IntervalBuilder): Interval {
    return new Interval(builder);
  }

  startAfter(other: Date) {
    return this.start > other;
  }

  endAtOrBefore(other: Date) {
    return this.end <= other;
  }

  minutesBeforeEndFrom(other: Date): Minutes {
    return Math.ceil(this.endDiffFrom(other) / 60000);
  }

  private endDiffFrom(other: Date): number {
    return this.end.getTime() - other.getTime();
  }
}

export interface Decomptes {
  launch: (timed: Consumer<Date>) => Unsubscribe;
}

const DecompteLauncherWithInterval = {
  of: (decompte: Decompte, decomptes: Decomptes) => {
    const remaining: Set<Time> = new Set();
    return {
      on: (timed: (time: Time) => void): Unsubscribe =>
        decomptes.launch((date) => {
          const time = decompte.withCurrent(date);
          if (remaining.has(time)) {
            return;
          }
          remaining.add(time);
          timed(time);
        }),
    };
  },
};

export const DecompteLauncher = {
  of: (decomptes: Decomptes) => ({
    withInterval: (interval: IntervalBuilder) => DecompteLauncherWithInterval.of(Decompte.of({ interval }), decomptes),
  }),
};

export interface DecompteBuilder {
  interval: IntervalBuilder;
}

export class Decompte {
  private readonly interval: Interval;
  private constructor(builder: DecompteBuilder) {
    this.interval = Interval.of(builder.interval);
  }

  static of(builder: DecompteBuilder): Decompte {
    return new Decompte(builder);
  }

  withCurrent(current: Date): Time {
    if (this.interval.startAfter(current)) {
      return NOT_STARTED;
    }
    if (this.interval.endAtOrBefore(current)) {
      return OVER;
    }
    return this.interval.minutesBeforeEndFrom(current);
  }
}
