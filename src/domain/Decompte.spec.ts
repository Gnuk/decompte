import { describe, it, expect } from 'vitest';

import { Consumer, Decompte, DecompteLauncher, Decomptes, IntervalBuilder, NOT_STARTED, OVER, Unsubscribe } from './Decompte';

const INTERVAL_EXAMPLE: IntervalBuilder = {
  start: new Date('2023-01-01T01:00:00.000Z'),
  end: new Date('2023-01-01T02:00:00.000Z'),
};
const DECOMPTE_EXAMPLE = Decompte.of({
  interval: INTERVAL_EXAMPLE,
});

class DecomptesInMemory implements Decomptes {
  private timerId = 0;
  private readonly timers: Map<number, Consumer<Date>> = new Map();
  launch(timed: Consumer<Date>): Unsubscribe {
    const id = ++this.timerId;
    this.timers.set(id, timed);
    return () => {
      this.timers.delete(id);
    };
  }

  push(date: Date): void {
    this.timers.forEach((timer) => timer(date));
  }
}
describe('DecompteLauncher', () => {
  it(
    'should receive time on emitted date',
    () =>
      new Promise<void>((done) => {
        const decomptes = new DecomptesInMemory();
        const launcher = DecompteLauncher.of(decomptes);
        const stop = launcher.withInterval(INTERVAL_EXAMPLE).on((time) => {
          expect(time).toBe(42);
          stop();
          done();
        });
        decomptes.push(new Date('2023-01-01T01:18:00.000Z'));
      }),
    {
      timeout: 100,
    }
  );
  it('should not send twice same time', () => {
    let count = 0;
    const decomptes = new DecomptesInMemory();
    const launcher = DecompteLauncher.of(decomptes);
    const stop = launcher.withInterval(INTERVAL_EXAMPLE).on(() => {
      count++;
    });

    decomptes.push(new Date('2023-01-01T01:17:00.000Z'));
    decomptes.push(new Date('2023-01-01T01:18:00.000Z'));
    decomptes.push(new Date('2023-01-01T01:18:00.000Z'));

    expect(count).toBe(2);
    stop();
  });
});

describe('Decompte', () => {
  it('should be over when current time is after interval', () => {
    expect(DECOMPTE_EXAMPLE.withCurrent(new Date('2023-01-01T02:01:00.000Z'))).toBe(OVER);
  });

  it('should be over when current time is at the end of the interval', () => {
    expect(DECOMPTE_EXAMPLE.withCurrent(new Date('2023-01-01T02:00:00.000Z'))).toBe(OVER);
  });

  it('should be not started when current time is before interval', () => {
    expect(DECOMPTE_EXAMPLE.withCurrent(new Date('2023-01-01T00:59:59.000Z'))).toBe(NOT_STARTED);
  });

  it.each([
    {
      current: '2023-01-01T01:59:00.000Z',
      minutes: 1,
    },
    {
      current: '2023-01-01T01:00:00.000Z',
      minutes: 60,
    },
    {
      current: '2023-01-01T01:57:00.001Z',
      minutes: 3,
    },
  ])(
    `should be $minutes for $current on the interval ${INTERVAL_EXAMPLE.start.toISOString()}, ${INTERVAL_EXAMPLE.end.toISOString()}`,
    ({ current, minutes }) => {
      expect(DECOMPTE_EXAMPLE.withCurrent(new Date(current))).toBe(minutes);
    }
  );
});
