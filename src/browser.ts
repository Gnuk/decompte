import { DecompteLauncher } from './domain/Decompte';
import { decompteWeb } from './infrastructure/primary/DecompteWeb';
import { DecomptesTimed } from './infrastructure/secondary/DecomptesTimed';

const decomptes = new DecomptesTimed();

const launcher = DecompteLauncher.of(decomptes);

const hashTime = document.location.hash.replace('#', '').split('--');

const decompteSelector = document.getElementById('decompte')!;
const errorSelector = document.getElementById('decompte-error')!;

if (hashTime.length !== 2) {
  const errorMessage = 'Please set an interval as an url hash like "#<start ISO>--<end ISO>", then reload the page.';
  errorSelector.innerText = errorMessage;
  throw new Error(errorMessage);
}

const [start, end] = hashTime;
launcher
  .withInterval({
    start: new Date(start),
    end: new Date(end),
  })
  .on(decompteWeb(decompteSelector));
