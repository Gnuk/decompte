# Decompte

Show a countdown.

## Prerequisites

* [Node](https://nodejs.org/) LTS

## Tests


To watch tests:

```shell
npm test
```

To watch tests with coverage:

```shell
npm run test:coverage
```

To run tests in continuous integration:

```shell
npm run test:ci
```
